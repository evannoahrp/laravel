<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

Route::get("/", function () {
  return view("home", [
    "title" => "Home"
  ]);
});

Route::get("/about", function () {
  return view("about", [
    "title" => "About",
    "name" => "Evannoah Rolimarch Pratama",
    "email" => "evannoah11@gmail.com",
    "image" => "image.jpg"
  ]);
});

Route::get("/posts", [PostController::class, "index"]);
Route::get("/posts/{slug}", [PostController::class, "show"]);