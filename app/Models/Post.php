<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;

class Post {
  private static $blog_posts = [
    [
      "title" => "Judul Post Pertama",
      "slug" => "judul-post-pertama",
      "author" => "Evannoah Rolimarch Pratama",
      "body" => "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odio architecto unde dolorum natus doloribus repellendus itaque libero cupiditate cumque cum, sequi hic fugit repudiandae quia! Consectetur doloribus eligendi sapiente quis nisi voluptas aliquid et velit voluptatum facere? Ut quam autem soluta veritatis minus quaerat voluptas, rerum eum quasi tempore sunt illum ipsam dolorum nesciunt quis deleniti nemo reiciendis et nam ipsum error fugiat maxime nihil laborum? Minus ex, quos commodi, deleniti ipsum explicabo laborum excepturi illum, quo iure ea amet?"
    ],
    [
      "title" => "Judul Post Kedua",
      "slug" => "judul-post-kedua",
      "author" => "Evannoah Rolimarch Pratama",
      "body" => "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Tempore maiores velit eos, vel provident repudiandae facilis maxime libero obcaecati dignissimos consequuntur fugit. Debitis nemo ducimus repellendus tenetur laboriosam voluptatem doloribus ipsam libero consectetur deserunt! Facere sequi voluptates incidunt amet quibusdam fugiat pariatur porro ut aspernatur rerum hic, numquam molestias! Eum aliquam, ipsum unde quas sit reiciendis consectetur perferendis cumque ad minima ipsam velit id porro corrupti, magnam totam debitis! Facere cupiditate ipsam dolor, laudantium provident sint hic, sit eaque dignissimos alias deleniti numquam veniam cumque aperiam porro eveniet. Sapiente odit temporibus enim voluptas illo, tempora eius fuga atque maxime quasi."
    ]
  ];

  public static function all() {
    return collect(self::$blog_posts);
  }

  public static function find($slug) {
    $posts = static::all();
    return $posts -> firstWhere("slug", $slug);
  }
}
