@extends('layouts.main')

@section('container')
  <article class="mb-5">
    <h2>{{ $post["title"] }}</h2>
    <h5>By: {{ $post["author"] }}</h5>
    <p>{{ $post["body"] }}</p>
  </article>
  <button type="button" class="btn btn-outline-primary" onclick="window.location.href='http://127.0.0.1:8000/posts'">Back to Posts</button>
@endsection